
import UIKit
import Firebase
import FirebaseRemoteConfig
import GoogleMobileAds

// Mobile Ads ####

//  Note :In this app I have divided 100 Post comming from server in to 20 Sections And Each Section  having 5 Rows of data.

//  I have fetched the banner ads ID from Firebase Remote Config and Displayed the Banner As In Each (20) Section Footer

//  Ad Id is being fetched from Firebase Remote config ie:       ca-app-pub-3940256099942544/6300978111

//  firebase key for ad Id : adMobId

// theBanner ad takes some time in Loading in the Section Footer

class ViewController: UIViewController , GADBannerViewDelegate, GADInterstitialDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    var viewModel = UserViewModel()
    var AdID : String  = ""
    var Adsize :String = ""
    var AdCnt = 0
    var remoteConfig = RemoteConfig.remoteConfig()
    var adheight = 0.0
        
    override func viewDidLoad() {
        
        super.viewDidLoad()
        fetchRemoteConfig()

        viewModel.getDataFromAPIHandlerClass(url: EndPoint.strUrl) { (_) in
            
            DispatchQueue.main.async { [weak self] in
                self?.tblView.reloadData()
            }
        }
    }
    
    func fetchRemoteConfig()
    {
        
        remoteConfig.fetch(withExpirationDuration: 0) {
            [unowned self] (status, error) in
            guard error == nil else { return }
            print("Got the value from Remote Config!")
            self.remoteConfig.activate()
            self.gettingValuesFromFirebase()
        }
    }
    
    func gettingValuesFromFirebase(){
        
        let adIdText = remoteConfig.configValue(forKey: "adMobId").stringValue ?? ""
        let size =  remoteConfig.configValue(forKey: "AdSize").stringValue ?? ""
        AdID = adIdText
        Adsize = size
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.bannerAdSize = Adsize
        
        tblView.reloadData()
        print("============ \(AdID)")
        print("Ad Size============ \(Adsize)")
        
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // Return the number of sections
        print("Section Count ----- \(viewModel.datasourceDictionary?.keys.count ?? 0)")
        return viewModel.datasourceDictionary?.keys.count ?? 0

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return 100
        
    }
    
    // MARK: - GADBannerViewDelegate methods
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView)
    {
        adheight = Double(bannerView.frame.size.height)
        
        print("Banner loaded successfully with height \(adheight)")
       
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        adheight = Double(bannerView.frame.size.height)
        print("Fail to receive ads")
        print(error)
    }
    
}

extension ViewController : UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            cell?.textLabel?.text = viewModel.getCellData(index: indexPath.row, section: indexPath.section)
            return cell!
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if adheight == 0 {
            return 50
        }
        else {
            return CGFloat(adheight)
        }
     
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "adCell") as! AdCell
        
        cell.adUnit = AdID
        cell.adBannerView!.adUnitID =  AdID
        cell.adBannerView!.load(GADRequest())
        cell.adBannerView!.delegate = self
        cell.adBannerView!.rootViewController = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController {
            vc.titleStr = viewModel.getCellData(index: indexPath.row, section: indexPath.section)
            let user = self.viewModel.getUserAtIndex(row: indexPath.row , section: indexPath.section)
            vc.bodyStr = user.body
            print("body === \(user.body ?? "")")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
