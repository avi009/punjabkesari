//
//  User.swift
//  punjabKesariDemoApp
//
//  Created by AVINASH on 08/03/21.
//  Copyright © 2021 AVINASH. All rights reserved.
//

import Foundation

class User : Codable{
    var userId : Int?
    var id : Int?
    var title : String?
    var body : String?
}
