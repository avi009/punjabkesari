//
//  AdCell.swift
//  punjabKesariApp
//
//  Created by Avinash Singh on 13/03/21.
//  Copyright © 2021 AVINASH. All rights reserved.
//

import UIKit
import GoogleMobileAds
class AdCell: UITableViewCell {
    var adBannerView : GADBannerView?
    var adUnit = ""
    var bannerAdSize = ""
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
            //  appDelegate.bannerAdSize = "aaaaa"
        
        print("banner ad size :\( appDelegate.bannerAdSize)")
        
        adBannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        self.addSubview(adBannerView!)
        self.selectionStyle = .none
        self.isUserInteractionEnabled = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
