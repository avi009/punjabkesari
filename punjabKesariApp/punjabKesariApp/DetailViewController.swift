//
//  DetailViewController.swift
//  punjabKesariApp
//
//  Created by AVINASH on 13/03/21.
//  Copyright © 2021 AVINASH. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var titleStr : String?
    var bodyStr : String?
    
    @IBOutlet weak var titleTxt: UILabel!
    
    
    @IBOutlet weak var bodyTxt: UITextView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let title = titleStr
        {
              print(" title : \(title)")
            
            titleTxt.text = title
            
        }
        
        if let body = bodyStr
        {
           print(" Body : \(body)")
            
            bodyTxt.text = body
        }
        
    }
    
}
