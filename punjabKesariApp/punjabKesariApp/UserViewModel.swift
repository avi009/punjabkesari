//
//  UserViewModel.swift
//  punjabKesariDemoApp
//
//  Created by AVINASH on 08/03/21.
//  Copyright © 2021 AVINASH. All rights reserved.
//

import Foundation


class UserViewModel{
    typealias completionBlock = ([User]) -> ()
    var apiHandler = APIHandler()
    var datasourceArray = [User]()
    var datasourceDictionary : [Int : [User]]?
    
   func getDataFromAPIHandlerClass(url: String, completionBlock : @escaping completionBlock){
    
    apiHandler.getDataFromApi(withUrl: url) { [weak self] (arrUser) in
        var arr = [User]()
        self?.datasourceDictionary = [Int : [User]]()
        for (index , item) in arrUser.enumerated() {
            arr.append(item)

            if (index+1) % 5 == 0 {
                print("count\(arr.count)")
                self!.datasourceDictionary?[index+1] = arr
                arr.removeAll()
            }
            
        }
        print(self!.datasourceDictionary)
        
        self?.datasourceArray = arrUser
        completionBlock(arrUser)
    }
    }
    
    func getNumberOfRowsInSection(section : Int) -> Int{
        if let datasourceDictionary = self.datasourceDictionary {
            let index = datasourceDictionary.index(datasourceDictionary.startIndex, offsetBy: section)
            let key = datasourceDictionary.keys[index]
            return  datasourceDictionary[key]!.count
        }
        
           return 0
    }
    
    func getNumberofSection() -> Int {
        return (self.datasourceDictionary?.keys.count)!
    }
    
    func getUserAtIndex(row : Int , section : Int) -> User{
        let index = self.datasourceDictionary!.index(self.datasourceDictionary!.startIndex, offsetBy: section)
        let key = self.datasourceDictionary!.keys[index]
        return  (self.datasourceDictionary?[key]![row])!
    }
    
    func getCellData(index : Int , section : Int) -> String{
        
        let user = self.getUserAtIndex(row: index , section: section)
        let id = user.id ?? 0
        let title = user.title ?? ""
        let res = " " + (title) + " "
        print(">>>>>>>>>>>>>>\(res)")
        return res
    }
}

