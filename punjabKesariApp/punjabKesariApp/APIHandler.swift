//
//  APIHandler.swift
//  punjabKesariDemoApp
//
//  Created by AVINASH on 08/03/21.
//  Copyright © 2021 AVINASH. All rights reserved.
//

import Foundation

class APIHandler{
    
    typealias completionBlock = ([User]) -> ()
    
    func getDataFromApi(withUrl strUrl : String, completionBlock : @escaping completionBlock){
        
        if let unwrappedUrl = URL(string: strUrl){
            
            URLSession.shared.dataTask(with: unwrappedUrl, completionHandler: { (data, response, error) in
                
                if data != nil{
                    let jsonDecoder = JSONDecoder()
                    let userArray = try? jsonDecoder.decode([User].self, from: data!)
                    
                    if userArray != nil{
                        completionBlock(userArray!)

                    }else{
                        let aArray = [User]()
                        completionBlock(aArray)
                    }
                }else{
                         let aArray = [User]()
                         completionBlock(aArray)
                }
                
            }).resume()
        }
    }
        
}
